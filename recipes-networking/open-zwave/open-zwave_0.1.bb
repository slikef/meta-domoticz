SUMMARY = "Open z-wave support"
SECTION = "networking"
LICENSE = "LGPL"

SRC_URI = "git://github.com/OpenZWave/open-zwave;protocol=https \
	   "
SRCREV = "fb2169b22f039f5b7f45344ec9244a615d4fc887"

S = "${WORKDIR}/git"

LIC_FILES_CHKSUM = "file://license/license.txt;md5=584c7ddacb8739db77ddcc47bd9d3b52"

DEPENDS += "udev coreutils-native"
