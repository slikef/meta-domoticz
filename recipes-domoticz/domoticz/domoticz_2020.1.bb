DESCRIPTION = "domoticz home automation system" 
SECTION = "base"
LICENSE = "GPL-3.0"
PR = "r0"

DEPENDS = "boost python3 curl libusb1"
RDEPENDS_${PN} += " rsync "

GIT_TAG = "2020.1"

SRC_URI = " \
	git://github.com/domoticz/domoticz.git;protocol=https;branch=master;tag=${GIT_TAG} \
	file://domoticz.conf \
	file://domoticz \
"
LIC_FILES_CHKSUM = "file://../git/License.txt;md5=d32239bcb673463ab874e80d47fae504"

S = "${WORKDIR}/git"
B = "${WORKDIR}/build"

EXTRA_OECMAKE += "-DCMAKE_BUILD_TYPE=Release CMakeLists.txt ../git"
inherit pkgconfig cmake

FILES_${PN} += " /opt/domoticz /opt/domoticz.v"

do_install() {
	install -d ${D}${bindir}
	install -d ${D}/var/www
	install -d ${D}/opt/domoticz
	install -d ${D}/opt/domoticz.v
	install -d ${D}/opt/domoticz/Config
	install -d ${D}/opt/domoticz/dzVents
	install -d ${D}/opt/domoticz/plugins
	install -d ${D}/opt/domoticz/scripts

	install -d ${D}${sysconfdir}/init.d/
	install -d ${D}${sysconfdir}/rc0.d/
	install -d ${D}${sysconfdir}/rc1.d/
	install -d ${D}${sysconfdir}/rc3.d/
	install -d ${D}${sysconfdir}/rc5.d/
	install -d ${D}${sysconfdir}/domoticz/
	
	install -m 0755 ${B}/domoticz ${D}${bindir}
	install -m 0644 ${S}/server_cert.pem ${D}${sysconfdir}/domoticz/
	install -m 0644 ${WORKDIR}/domoticz.conf ${D}${sysconfdir}/domoticz/
	cp -R ${S}/Config/* ${D}/opt/domoticz/Config
	cp -R ${S}/dzVents/* ${D}/opt/domoticz/dzVents/
	cp -R ${S}/plugins/* ${D}/opt/domoticz/plugins/
	cp -R ${S}/scripts/* ${D}/opt/domoticz/scripts/
	cp -R ${S}/www/* ${D}/var/www/

	install -m 0755 ${WORKDIR}/domoticz ${D}${sysconfdir}/init.d/
	ln -s ../init.d/domoticz ${D}${sysconfdir}/rc0.d/K01domoticz
	ln -s ../init.d/domoticz ${D}${sysconfdir}/rc1.d/K01domoticz
	ln -s ../init.d/domoticz ${D}${sysconfdir}/rc3.d/K01domoticz
	ln -s ../init.d/domoticz ${D}${sysconfdir}/rc5.d/S95domoticz
}
