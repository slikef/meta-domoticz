SUMMARY = "Configuration of USB composite gadget"
SECTION = "connectivity"
LICENSE = "CLOSED"
PR = "r3"

RDEPENDS_${PN} = " bash "

SRC_URI = " \
	file://usb-configfs \
	http://10.20.1.195:8080/usb.img;name=usbdisk \
	"

SRC_URI[usbdisk.md5sum] = "d0f1d0f4422b36b252a0fc15e554ff43"

do_compile() {
}

do_install() {
	install -d ${D}/opt/g_multi
	install -d ${D}${sysconfdir}/init.d
	install -d ${D}${sysconfdir}/rc5.d

	install -m 0664 ${WORKDIR}/usb.img ${D}/opt/g_multi/

	install -m 0755 ${WORKDIR}/usb-configfs ${D}${sysconfdir}/init.d/
	ln -sf ../init.d/usb-configfs ${D}${sysconfdir}/rc5.d/S91usb-configfs
}

FILES_${PN} += " /opt "

