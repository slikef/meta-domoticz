SUMMARY = "A console-only image that fully supports the target device \
hardware."

LICENSE = "MIT"

inherit core-image

MACHINE_FEATURES_remove = " bluetooth usbhost touchscreen screen alsa "
DISTRO_FEATURES_remove = " acl alsa bluetooth irda pcmcia usbgadget usbhost nfs 3g nfc x11 opengl wayland pulseaudio bluez5 "
DISTRO_FEATURES_DEFAULT_remove = " bluetooth "
DISTRO_FEATURES_BACKFILL_remove = " bluez5 "

IMAGE_INSTALL_append = " \
	curl \
	domoticz \
	python3 \
	python3-dev \
	lua \
	i2c-tools \
	initscripts \
	python-smbus \
	bridge-utils \
	iptables \
	wpa-supplicant \
	netcat \
	ntp \
	zeromq \
	screen \
	iproute2 \
	openssh \
	openssl \
	bash \
	tzdata \
	tzdata-europe \
	unzip \
	wget \
	zerotier \
	tree \
"

PREFERRED_VERSION_boost = "1.65.0"
